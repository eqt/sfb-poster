#!/bin/bash
#
#
#
TEXLIVE_HOME="/app/unido-i08/lix86_2.6.64/texlive/2015"
PATH="${TEXLIVE_HOME}/bin/x86_64-linux:${PATH}"
MANPATH="${TEXLIVE_HOME}/texmf-dist/doc/man" 
INFOPATH="${TEXLIVE_HOME}/texmf-dist/doc/info"
export TEXLIVE_HOME
export PATH
export MANPATH
export INFOPATH
