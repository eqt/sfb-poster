SRC_POSTER=SFB876_3_Poster
# Figure out which machine we're running on.
UNAME=$(shell uname -s)
DIR=$(shell pwd)

all:
	    $(MAKE) -C Projects/C1
	    $(MAKE) -C Projects/examples

poster:
	    $(MAKE) -C Projects/C1 poster
	    $(MAKE) -C Projects/examples poster

wo-java:
	    $(MAKE) -C Projects/C1 wo-java
	    $(MAKE) -C Projects/examples wo-java

clean:
	    $(MAKE) -C Projects/C1 clean
	    $(MAKE) -C Projects/examples clean

clean-deep:
	    $(MAKE) -C Projects/C1 clean-deep
	    $(MAKE) -C Projects/examples clean-deep

batch:
	pdflatex $(SRC_POSTER)
	bibtex8 -B $(SRC_POSTER) || if [ $$? -ne 1 ] ; then $$?; fi

	pdflatex $(SRC_POSTER)
	pdflatex $(SRC_POSTER)
	pdflatex $(SRC_POSTER)

clean-batch:
	rm -f *.aux *.lof *.log *.lot *.fls *.out *.toc *.fmt *.idx *.ilg *.ind *.ist *.bbl *.cut *.nav *.snm
	rm -f *.bcf *.blg *-blx.aux *-blx.bib *.run.xml *.upa *.upb *.fdb_latexmk *.synctex
	rm -f *.synctex\(busy\) *.synctex.gz *.synctex.gz\(busy\) *.pdfsync

.PHONY: clean poster
