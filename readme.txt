SFB876-3-Poster:

This repository provides a template for the design of the SFB876-3-Posters.
The template is intended to be used as a baseline for each part project.
If any question, issues, or package requests arise please feel free to send a mail
to danny.heinrich@tu-dortmund.de

Getting Started:

You can start editing the poster of your respective project by going to "./Projects/<ProjectName>".
There, you will find a main.tex as well as a Makefile for Linux and Windows, which compiles your poster as well as the folder "Config" 
and for each poster a main, poster and content file, i.e. Filename_A.tex or Filename_B.tex.
The Config folder contains a config file (config_A.tex, config_B.tex) for each poster as well as header and footer files, which are the same for both posters.

	Config.tex:
		The Config File contains the relative paths to content, header and footer if you decide to change their location.
		Furthermore, the poster headers can be changed here, which allows to adjust project name, title and project managers in the given format.
		You can also edit the basic layout of the poster, i.e, row height and column width for each row based on the A0 format (DIN A0 = 118.9cm x 84.1cm).

	Content.tex
		In the Content File you first define the number of columns each row, ranging from 1 to 3.
		For example "\problemColumns{2}" creates two columns in the first row.
		Depending on the number of columns, box environments are defined as follows:
		The left box is always defined, while the right box is defined for two and three columns, with the center box
		being added if you choose a three column layout. Choosing a three column layout for the "Problem Row" 
		problemBoxLeft, problemBoxRight and problemBoxCenter can be used. Keep in mind, that once you reduce the number of columns
		for a row, some boxes may no longer be defined and have to be removed/commented out for the main file to compile properly.

	Header.tex
		The header contains two dummy tcolorboxes, that may be replaced with an associate or partner logo. 
		Simply replace the tcolorboxes with includegraphics or figure environments.
		If there is no partner logo to be placed you can just delete the boxes.
		The logos should be placed beneath the TU Logo and if needed adjust the coordinates "at (x,y)"
		to move the logos.

	Footer.tex
		The footer logos should contain the chair logos associated with the project. The style follows the same pattern as
		the header. Simply replace the tcolorboxes with images (preferably vector graphics) and delete unnecessary boxes.

Page Margin and Draft:
	The gray margin on the poster indicate the print margin. No elements should cross that margin. 
	The yellow margins have a width of 0.8 cm and while box borders may overlap with the yellow margin, 
	objects and text should not. You can disable both draft and print margin by commenting out the subimports "draft" and "print_margin"
 	in Poster_A.tex and Poster_B.tex.

Examples:
	The Folder "./Projects/examples" contains an example Poster, 
	where the input of content was split up into an additional "Content" folder.
	Feel free to copy and adjust the example with your own content. The provided examples are
	suggestions on how to visualize tables, matrices, images and equations. Feel free to edit or adjust
	according to your specifications.
	
Commands:
	
	Several commands have been defined to provide useful functions.
	All commands are used in the example poster. Here [x] are the number of arguments that need to be provided for each command.
	There are a number of SIUnit such as \MHz and math commands such as \argmin or \argmax already defined. See "./Config/commands.tex" 
	for a full list of commands.

	\pt[1]
		Set font size to specified number in pt: e.g. \pt{32} to set font size of following text to 32 pt.

	\ptcol[3]
		Set font size to specified number AND colorize text: e.g. \ptcol{TUgreen}{32}{Lorem Ipsum} produces a TUgreen text of size 32pt

	\textref[1]
		Standardized citation command. Displays citation in a specific format: e.g. \textref{\cite{Buschjaeger/Morik/2017b}} results
		in a colored citation surrounded by parantheses.

	\mathcolorbox[2]
		Surrounds an equation or part of an equation by a colorbox with a custom color. For example 
		\mathcolorbox{Steelgray}{a^2 + b^2 = c^2} results in an equation with a Steelgray background. 

	\project[3]
		Display a circle contaning text specified by the user. This is used for displaying and referencing other projects.
		For example \project{A1}{15}{10} displays 'A1' at position (x,y) 15 and 10 in cm relative to the box position.
	
Boxes:
	picturebox[2]
		Custom Tcolorbox that allows a to set a background image and its opacity. The first argument should be the path to the image and the second argument its
		desired opacity. Additionally it supports the same parameters as any tcolorbox would (e.g [colframe=white]).
		\begin{picturebox}[colframe=red, colback=white]{Images/background}{0.4}
			Lorem Ipsum ...
		\end{picturebox}
		Extending the original definition of picturebox can be achieved by creating a new tcolorbox and adjusting the parameters as necessary (see template/custom_boxes.tex).

Additional Options for Poster A:

	For poster A you may choose to merge the bottom two rows into a single one. 
	Adding \mergebottrue 
	to the config will merge the lower two rows similar to the layout of poster B. 
	The merge is set to \mergebotfalse by default.
	Furthermore, hiding the horizontal spacer between the two merged rows is also possible.
	This can be achieved simply by adding \researchborderfalse to the config file. 
	After hiding the horizontal spacer you can use

	\begin{plannedResearchBox}
		%Your Content
	\end{plannedResearchBox}

	as a single box instead of multiple boxes. Examples are located in the "add_examples" folder.

Additional Options for Poster B:

	If you want to have only a single box for the "Planned Research" Row in Poster B you can disable the separator by adding 
	\researchborderfalse
	to your Config_B file.
	You can then use the plannedResearchBox as usual, i.e.: 

	\begin{plannedResearchBox}
	    %Your Content here
	\end{plannedResearchBox}

	You may also choose to merge the top two rows instead using \mergetoptrue. This will merge the two top rows instead and the "plannedResearchBox" may be used in the same fashion if researchborder is set to false.

	As merging different rows may result in the need for different names you can also rename each row using the following command redifinitions:

	%Custom Names for the Header Boxes
	\renewcommand{\botName}{Results}
	\renewcommand{\midName}{Methodology}
	\renewcommand{\topName}{Problem}
	\renewcommand{\mergeBotName}{Methodology and Results}
	\renewcommand{\mergeTopName}{Some Custom Title}

	Note that mergeBotName and mergeTopName are only used if either the two top rows or the bottom rows are merged. Examples, again, can be found in the add_example folder and more specifically in the config portion.

Box Alignment:
	
	You can also align boxes at the top of each row by wrapping a minipage around your content. This limits the height of the box and aligns
	your content at the top of the box.

	\begin{methodologyBoxLeft}
		\begin{minipage}[t][\methodologyRowHeight-2\horizontalSepWidth][t]{\textwidth}
			 %Your Content Here
		\end{minipage}
	\end{methodologyBoxLeft}

	The minipage limits the size of the box based on the row height specified in the config file minus the separator width.
	For problem- and resultsBoxes \methodologyRowHeight should be substituted with \problemRowHeight or \resultsRowHeight respectively.
